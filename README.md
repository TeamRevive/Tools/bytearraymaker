# ByteArrayMaker

### What is ByteArrayMaker?
ByteArrayMaker aims to convert cheatengines format to normal byte arrays with commas and 0x so it can be used in c# arrays.

### Download
you can find a compiled version [here](https://gitlab.com/TeamRevive/Tools/bytearraymaker/-/tags/)

### Screenshot
![alt text](screenshots/screenshot.jpg "screenshot")