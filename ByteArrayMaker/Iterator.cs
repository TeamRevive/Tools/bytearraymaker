﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ByteArrayMaker
{
    public class Iterator<T>
    {
        private List<string> list;
        private int index = -1;

        public Iterator(List<string> list)
        {
            this.list = list;
        }
        
        public bool HasNext()
        {
            int has = (index + 1);

            if(has < this.list.Count)
            {
                return true;
            }
            return false;
        }

        public string Next()
        {
            this.index = (this.index + 1);
            return this.list[index];
        }

        public void Remove()
        {
            this.list.RemoveAt(index);
            this.index = index - 1; //decrement.
        }

    }
}
