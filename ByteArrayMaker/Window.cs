﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByteArrayMaker
{
    public partial class Window : Form
    {
        public Window()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.bytetextbox.Text != String.Empty)
            {
                Regex r = new Regex(@"^([0-9A-F]{2})+?(\s[0-9A-F]{2})*$");

                if(r.IsMatch(this.bytetextbox.Text.ToUpper()))
                {

                    List<String> bytelist = new List<String>();

                    String[] ar = this.bytetextbox.Text.Split(' ');
                    foreach(String dbyte in ar)
                    {
                        bytelist.Add(dbyte);
                    }

                    StringBuilder b = new StringBuilder();

                    Iterator<string> it = new Iterator<string>(bytelist);
                    bool cancel = false;

                    while(true)
                    {
                        for(int i = 0; i < 5; i++)
                        {
                            if(it.HasNext())
                            {
                                string bytea = it.Next();
                                b.Append("0x"+bytea);
                                if(it.HasNext())
                                {
                                    b.Append(",");
                                }
                            } else
                            {
                                cancel = true;
                                break;
                            }
                        }

                        if(cancel)
                        {
                            break;
                        }

                        b.AppendLine();
                    }

                    this.result.Clear();
                    this.result.Text = b.ToString();

                } else
                {
                    MessageBox.Show("A array should be having the following syntax:\nAB 9C B1", "Invalid array string", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            } else
            {
                MessageBox.Show("Cannot a array which is empty", "empty array", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Window_Load(object sender, EventArgs e)
        {

        }

        private void copybtn_Click(object sender, EventArgs e)
        {
            if (result.Text == String.Empty)
            {
                MessageBox.Show("no result found", "no result", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Clipboard.SetText(this.result.Text, TextDataFormat.Text);
                MessageBox.Show("the result has been copied!", "copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
